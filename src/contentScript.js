const PREFIX = 'https://devsrvui.glassboxrnd.com:8443/webinterface/';

let version = '0.0.1';
let branchName;

console.debug('version: ', version);

chrome.runtime.onMessage.addListener(async function (request, sender, sendResponse) {
    await getBranchName();
    createButtons();
});


function createButtons() {
    if (document.body.querySelector('.buttonsWrapper')) {
        return;
    }

    const webuiButton = createButton('Webui', 'webui');
    const webuitoolsButton = createButton('Webuitools', 'webuitools');
    //const runPipelineButton = createPipelineButton();

    const buttonsWrapper = wrapButtons(webuiButton, webuitoolsButton);

    addButtonsToPage(buttonsWrapper);
}

function wrapButtons() {
    const buttonsWrapper = document.createElement('div');
    buttonsWrapper.classList.add('buttonsWrapper');
    for (let i = 0; i < arguments.length; i++) {
        buttonsWrapper.appendChild(arguments[i]);
    }

    return buttonsWrapper;
}

function getBranchName() {
    return new Promise((resolve, reject) => {
        const intervalID = setInterval(() => {
            branchName = document.querySelector('[data-qa="pr-branches-and-state-styles"] [role="button"] div');
            if (branchName) {
                clearInterval(intervalID);
                branchName = editBranchName(branchName.innerText);
                resolve();
            }
        }, 300);
    });
}

function addButtonsToPage(buttonsWrapper) {
    let buttonsContainer;

    const intervalID = setInterval(() => {
        buttonsContainer = document.body.querySelector('.css-vxcmzt');
        if (buttonsContainer) {
            clearInterval(intervalID);
            const referenceElement = document.body.querySelectorAll('.css-z25nd1')[4];
            buttonsContainer.insertBefore(buttonsWrapper, referenceElement)
        }
    }, 300);
}

function createButton(text, value) {
    const button = document.createElement('a');
    button.classList.add('gb-button');
    button.href = PREFIX + value + '_' + branchName;
    button.innerText = text;
    button.addEventListener('click', (e) => {
        e.preventDefault();
        copyToClipboard(PREFIX + value + '_' + branchName);
        addBubble()
    })

    const copyIcon = document.createElement('div');
    copyIcon.classList.add('copy');
    button.appendChild(copyIcon);

    return button;
}

function createPipelineButton() {
    const button = document.createElement('div');
    button.classList.add('gb-button');
    button.innerText = 'Run Pipeline';
    button.addEventListener('click', (e) => {
        runPipeline();
    })
    return button;
}

function addBubble() {
    const bubble = document.createElement('div');
    bubble.classList.add('speech-bubble');
    bubble.innerText = 'Copied To Clipboard';
    const reviewerImage = document.body.querySelector('.css-mdx98q');
    reviewerImage.style.position = 'relative';
    reviewerImage.appendChild(bubble);

    setTimeout(() => {
        bubble.classList.add('added');
    }, 0);

    setTimeout(() => {
        bubble.classList.remove('added');
    }, 2000)
}

function editBranchName(branchName) {
    return branchName.replace(/\//g, '_');
}

function copyToClipboard(str) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected = document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
        document.getSelection().removeAllRanges();
        document.getSelection().addRange(selected);
    }
}

function runPipeline() {
    postPayload.target.ref_name = branchName;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", pipelineUrl, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(postPayload));
}

const postPayload = {
    target: {
        ref_name: "GLAS30-7715-funnel-export-to-pdf-or-excel-no-widget-name-exported",
        ref_type: "branch",
        selector: {"type": "custom", "pattern": "webui"},
        type: "pipeline_ref_target"
    }
}
